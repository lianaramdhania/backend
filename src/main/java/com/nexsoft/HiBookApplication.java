package com.nexsoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HiBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(HiBookApplication.class, args);
    }
}
