package com.nexsoft.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "pinjam")
public class Pinjam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 4)
    private int id;

    @ManyToOne
    @JoinColumn(name = "penggunaId")
    Pengguna pengguna;

    @ManyToOne
    @JoinColumn(name = "produkId")
    Produk produk;

    @ManyToOne
    @JoinColumn(name= "laporanId")
    Laporan laporan;

    @Column(nullable = true, length = 100)
    private String tanggalPinjam = "";

    @Column(nullable = false, length = 20)
    private int biaya;

    @Column(nullable = false, length = 30)
    private String statusBuku;

    public Pengguna getPengguna() {
        return pengguna;
    }

    public void setPengguna(Pengguna pengguna) {
        this.pengguna = pengguna;
    }

    public Produk getProduk() {
        return produk;
    }

    public void setProduk(Produk produk) {
        this.produk = produk;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTanggalPinjam() {
        return tanggalPinjam;
    }

    public void setTanggalPinjam(String tanggalPinjam) {
        this.tanggalPinjam = tanggalPinjam;
    }

    public int getBiaya() {
        return biaya;
    }

    public void setBiaya(int biaya) {
        this.biaya = biaya;
    }

    public String getStatusBuku() {
        return statusBuku;
    }

    public void setStatusBuku(String statusBuku) {
        this.statusBuku = statusBuku;
    }

    @Override
    public String toString() {
        return "Pinjam{" +
                "id=" + id +
                ", pengguna=" + pengguna +
                ", produk=" + produk +
                ", tanggalPinjam='" + tanggalPinjam + '\'' +
                ", biaya=" + biaya +
                ", statusBuku='" + statusBuku + '\'' +
                '}';
    }

    public Laporan getLaporan() {
        return laporan;
    }

    public void setLaporan(Laporan laporan) {
        this.laporan = laporan;
    }
}
