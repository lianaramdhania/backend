package com.nexsoft.service;

import com.nexsoft.entity.Pengguna;
import com.nexsoft.repository.PenggunaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PenggunaService {
    @Autowired
    private PenggunaRepository penggunaRepository;

    public Pengguna savePengguna(Pengguna pengguna){
        return penggunaRepository.save(pengguna);
    }

    public List<Pengguna> getPengguna() {
        return penggunaRepository.findAll();
    }

    public String deletePengguna(int id_pengguna) {
        penggunaRepository.deleteById(id_pengguna);
        return "PENGGUNA DIHAPUS";
    }

    public Pengguna updatePengguna(Pengguna pengguna) {
        Pengguna existingPengguna = penggunaRepository.findById(pengguna.getId_pengguna()).orElse(null);
        existingPengguna.setId_pengguna(pengguna.getId_pengguna());
        existingPengguna.setUsername(pengguna.getUsername());
        existingPengguna.setNama(pengguna.getNama());
        existingPengguna.setAlamat(pengguna.getAlamat());
        existingPengguna.setEmail(pengguna.getEmail());
        existingPengguna.setNomorHp(pengguna.getNomorHp());
        existingPengguna.setTanggalRegis(pengguna.getTanggalRegis());
        existingPengguna.setStatus(pengguna.getStatus());
        return penggunaRepository.save(existingPengguna);
    }

    public Pengguna ubahPassword(Pengguna pengguna){
        Pengguna existingPengguna = penggunaRepository.findById(pengguna.getId_pengguna()).orElse(null);
        existingPengguna.setPassword(pengguna.getPassword());
        return penggunaRepository.save(existingPengguna);
    }
}
