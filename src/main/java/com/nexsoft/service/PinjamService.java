package com.nexsoft.service;

import com.nexsoft.entity.Pinjam;
import com.nexsoft.repository.PinjamRepository;
import com.nexsoft.repository.ProdukRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PinjamService {
    @Autowired
    private PinjamRepository pinjamRepository;

    @Autowired
    private ProdukRepository produkRepository;

    public Pinjam savePinjam(Pinjam pinjam){
        return pinjamRepository.save(pinjam);
    }

    public List<Pinjam> getPinjam() {
        return pinjamRepository.findAll();
    }

    public String deletePinjam(int id, int idBuku) {
        pinjamRepository.hapusKeranjang(id, idBuku);
        return "LIST PEMINJAMAN DIHAPUS";
    }

    public List<Pinjam> updatePinjam(Pinjam pinjam) {
        List<Pinjam> existingPinjam = pinjamRepository.getDataKeranjangByIdPengguna(pinjam.getPengguna().getId_pengguna());
        List<Pinjam> editedPinjam = new ArrayList<>();
        System.out.println(existingPinjam);
        for(Pinjam pinjam1 : existingPinjam)
        {

            pinjam1.setTanggalPinjam(pinjam.getTanggalPinjam());
            pinjam1.setBiaya(pinjam.getBiaya());
            pinjam1.setStatusBuku("pinjam");

            editedPinjam.add(pinjamRepository.save(pinjam1));
        }
        return editedPinjam;
    }


//    public String updatePinjam1(Pinjam pinjam) {
//        List<Pinjam> existingPinjam = pinjamRepository.getDataKeranjangByIdPengguna(pinjam.getPengguna().getId_pengguna());
//        List<Pinjam> editedPinjam = new ArrayList<>();
//        String response="";
//        System.out.println(existingPinjam);
//        System.out.println("haihai"+produkRepository.getNovel());
//        if(produkRepository.getNovel() <= 4) {
//            for (Pinjam pinjam1 : existingPinjam) {
//
//                pinjam1.setTanggalPinjam(pinjam.getTanggalPinjam());
//                pinjam1.setBiaya(pinjam.getBiaya());
//                pinjam1.setStatusBuku("pinjam");
//
//                editedPinjam.add(pinjamRepository.save(pinjam1));
//            }
//            return "";
//        }
//        if(produkRepository.getNovel() <= 4){
//            editedPinjam.add(pinjamRepository.save(pinjam1));
//            response= "BERHASIL PINJAM";
//        }

//        List<Pinjam> existingPinjam = pinjamRepository.getDataKeranjangByIdPengguna(pinjam.getPengguna().getId_pengguna());
//        List<Pinjam> editedPinjam = new ArrayList<>();
//        System.out.println(existingPinjam);
//        for(Pinjam pinjam1 : existingPinjam)
//        {
//
//            pinjam1.setTanggalPinjam(pinjam.getTanggalPinjam());
//            pinjam1.setBiaya(pinjam.getBiaya());
//            pinjam1.setStatusBuku("pinjam");
//
//            editedPinjam.add(pinjamRepository.save(pinjam1));
//        }

//        return "editedPinjam";
//    }
}
