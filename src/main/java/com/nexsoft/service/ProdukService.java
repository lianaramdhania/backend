package com.nexsoft.service;

import com.nexsoft.entity.Laporan;
import com.nexsoft.entity.Pengguna;
import com.nexsoft.entity.Pinjam;
import com.nexsoft.entity.Produk;
import com.nexsoft.repository.PinjamRepository;
import com.nexsoft.repository.ProdukRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdukService {
    @Autowired
    private ProdukRepository produkRepository;

    @Autowired
    private PinjamRepository pinjamRepository;

    public String saveProduk(Produk produk) {
        String response = "";
            if (produk.getIdBuku().equals(produkRepository.getIdBukuOnly(produk.getIdBuku()))) {
                response = "GAGAL TAMBAH BUKU, ID SUDAH ADA, GANTI";
            } else if(produk.getJudulBuku().equals(produkRepository.getJudulBukuu(produk.getJudulBuku()))) {
                response = "GAGAL TAMBAH BUKU, JUDUL BUKU SUDAH ADA, GANTI";
            }else{
                produkRepository.save(produk);
                response = "BERHASIL";
            }
            return response;
        }

    public List<Produk> getProduk() {
        return produkRepository.getSemuaProduk();
    }

    public String deleteProduk(int id) {
        produkRepository.deleteById(id);
        return "PRODUK DIHAPUS";
    }

    public List<Produk> searchProduk(String search){
        return produkRepository.searchProduk(search);
    }

    public Produk updateProduk(Produk produk) {
        Produk existingProduk = produkRepository.findById(produk.getId()).orElse(null);
        existingProduk.setIdBuku(produk.getIdBuku());
        existingProduk.setJudulBuku(produk.getJudulBuku());
        existingProduk.setNamaPengarang(produk.getNamaPengarang());
        existingProduk.setJenisBuku(produk.getJenisBuku());
        existingProduk.setJumlahBuku(produk.getJumlahBuku());
        existingProduk.setHargaSewa(produk.getHargaSewa());
        existingProduk.setKeterangan(produk.getKeterangan());
        return produkRepository.save(existingProduk);
    }

    public String hapusProduk(Produk produk){
        Produk existingProduk = produkRepository.findById(produk.getId()).orElse(null);
        List<Pinjam> dataAll = pinjamRepository.findAll();
        Produk status = null;
        int cek1 = 0;
        for (Pinjam element: dataAll) {
            if(element.getId()==produk.getId()){
                cek1 = 1;
            }
        }

        if(cek1==0){
            existingProduk.setTerhapus(produk.getTerhapus());
            status = produkRepository.save(existingProduk);
        }else {
            return "ERROR";
        }
        return "BERHASIL";
    }

//    public String cekProduk(Produk produk) {
//        String response = "";
//        if(produkRepository.getNovel() <= 4){
//            response =
//        }
//    }
}
