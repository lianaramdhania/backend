package com.nexsoft.service;

import com.nexsoft.entity.Laporan;
import com.nexsoft.repository.LaporanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LaporanService {
    @Autowired
    private LaporanRepository laporanRepository;

    public Laporan saveLaporan(Laporan laporan){
        return laporanRepository.save(laporan);
    }

    public List<Laporan> getLaporan() {
        return laporanRepository.findAll();
    }

    public String deleteLaporan(int id) {
        laporanRepository.deleteById(id);
        return "LAPORAN DIHAPUS";
    }

    public Laporan updateLaporan(Laporan laporan) {
        Laporan existingLaporan = laporanRepository.findById(laporan.getId()).orElse(null);
        existingLaporan.setDenda(laporan.getDenda());
        existingLaporan.setTotal(laporan.getTotal());
        existingLaporan.setTanggalKembali(laporan.getTanggalKembali());
        return laporanRepository.save(existingLaporan);
    }
}
