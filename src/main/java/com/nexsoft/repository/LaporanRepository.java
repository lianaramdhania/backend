package com.nexsoft.repository;

import com.nexsoft.entity.Laporan;
import com.nexsoft.entity.Pinjam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LaporanRepository extends JpaRepository<Laporan, Integer> {
    @Query("SELECT u FROM Laporan AS u where u.id=:id")
    public List<Laporan> getDataLaporanById(@Param("id") int id);


    @Query(value = "SELECT * FROM pinjam INNER JOIN produk ON pinjam.produk_id = produk.id WHERE pinjam.pengguna_id = :pengguna_id", nativeQuery = true)
    public List<Pinjam> getDataLaporanByIdPinjam(@Param("pengguna_id") int pengguna_id);

//    @Query(value= "SELECT * FROM pinjam where pinjam.id =: pinjam.id", nativeQuery = true)

    //admin
    @Query(value = "SELECT sum(total) FROM laporan WHERE tanggal_kembali LIKE :tanggal_kembali%", nativeQuery = true)
    Integer getPenghasilanPerhari(@Param("tanggal_kembali") String tanggal_kembali);

    @Query(value = "SELECT count(id) FROM pinjam WHERE status_buku = 'pinjam' AND tanggal_pinjam LIKE :tanggal_pinjam%", nativeQuery = true)
    Integer getBukuTerpinjam(@Param("tanggal_pinjam") String tanggal_pinjam);

    @Query(value = "SELECT count(id) FROM pinjam WHERE status_buku = 'kembali' AND tanggal_kembali LIKE :tanggal_kembali%", nativeQuery = true)
    Integer getBukuDikembalikkan(@Param("tanggal_kembali") String tanggal_kembali);

    //pengguna
    @Query(value = "SELECT count(status_buku) FROM pinjam WHERE status_buku = 'kembali' AND pengguna_id =:pengguna_id", nativeQuery = true)
    Integer getJumlahDikembalikanByIdPengguna(@Param("pengguna_id") int pengguna_id);

    @Query(value = "SELECT count(status_buku) FROM pinjam WHERE status_buku = 'pinjam' AND pengguna_id =:pengguna_id", nativeQuery = true)
    Integer getJumlahDipinjamByIdPengguna(@Param("pengguna_id") int pengguna_id);

    @Query(value = "SELECT sum(total) FROM pinjam WHERE status_buku = 'pinjam' AND pengguna_id =:pengguna_id", nativeQuery = true)
    Integer getBiaya(@Param("pengguna_id") int pengguna_id);
}
