package com.nexsoft.repository;

import com.nexsoft.entity.Pinjam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface PinjamRepository extends JpaRepository<Pinjam, Integer> {
    @Query("SELECT u FROM Pinjam AS u where u.id=:id")
    public List<Pinjam> getDataPinjamById(@Param("id") int id);

    @Query(value = "SELECT * FROM pinjam WHERE pengguna_id=:pengguna_id", nativeQuery = true)
    public List<Pinjam> getDataPinjamByIdPengguna(@Param("pengguna_id") int pengguna_id);

    @Query(value = "SELECT * FROM pinjam WHERE pengguna_id=:pengguna_id AND status_buku='keranjang'", nativeQuery = true)
    public List<Pinjam> getDataPinjamByIdPenggunadanStatus(@Param("pengguna_id") int pengguna_id);

//Untuk show di keranjang
    @Query(value = "SELECT * FROM pinjam INNER JOIN produk ON pinjam.produk_id = produk.id WHERE pinjam.pengguna_id = :pengguna_id", nativeQuery = true)
    public List<Pinjam> getDataKeranjangByIdPengguna(@Param("pengguna_id") int pengguna_id);

//    @Query(value = "SELECT * FROM pinjam INNER JOIN laporan ON laporan.laporan_id = laporan.id WHERE pinjam.pengguna_id = :pengguna_id", nativeQuery = true)

    @Query(value = "SELECT count(produk_id) FROM pinjam WHERE produk_id =:produk_id AND pengguna_id=:pengguna_id AND status_buku = 'keranjang'", nativeQuery = true)
    public Integer getJumlahProduk(@Param("produk_id") int produk_id, @Param("pengguna_id") int pengguna_id);

    @Query(value = "SELECT * FROM pinjam WHERE status_buku='pinjam'", nativeQuery = true)
    public List<Pinjam> getDataPengembalian();

    //UNTUK SHOW LAPORAN DI ADMIN
    @Query(value = "SELECT * FROM pinjam WHERE status_buku = 'kembali'", nativeQuery = true)
    public List<Pinjam> getLaporanByStatus();

//
//    @Modifying
//    @Transactional
//    @Query(value = "UPDATE pinjam SET status_buku = 'kembali' WHERE pengguna_id = :id_pengguna AND status_buku = 'pinjam'", nativeQuery = true)
//    public Integer updateStatusBukuByPenggunaId(@Param("id_pengguna") int id_pengguna);

    @Modifying
    @Transactional
    @Query(value = "UPDATE pinjam SET laporan_id = :idLaporan, status_buku = 'kembali' WHERE pengguna_id = :id_pengguna AND status_buku = 'pinjam'", nativeQuery = true)
    public void updateIdLaporan(@Param("idLaporan") int idLaporan, @Param("id_pengguna") int id_pengguna);

//    @Modifying
//    @Transactional
//    @Query(value = "UPDATE pinjam SET laporan_id = :idLaporan, status_buku = 'kembali' WHERE pengguna_id = :id_pengguna AND status_buku = 'pinjam'", nativeQuery = true)
//    public void hapusKeranjang(@Param("idLaporan") int idLaporan, @Param("id_pengguna") int id_pengguna);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM pinjam WHERE id=:id AND produk_id=:produk_id", nativeQuery = true)
    public void hapusKeranjang(@Param("id") int id, @Param("produk_id") int produk_id);

    //BALIKIN STOK
    @Query(value = "SELECT produk_id FROM pinjam WHERE id =:id", nativeQuery = true)
    public Integer pilihProdukDariId(@Param("id") int id);

}

