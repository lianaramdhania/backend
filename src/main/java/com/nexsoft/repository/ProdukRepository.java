package com.nexsoft.repository;

import com.nexsoft.entity.Produk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface ProdukRepository extends JpaRepository<Produk, Integer> {
    @Query("SELECT u FROM Produk AS u where u.id=:id")
    public List<Produk> getDataProdukById(@Param("id") int id);
    @Query("SELECT u FROM Produk AS u where u.namaPengarang=:namaPengarang")
    public List<Produk> getDataProdukByNamaPengarang(@Param("namaPengarang") String namaPengarang);
    @Query("SELECT u FROM Produk AS u where u.judulBuku=:judulBuku")
    public List<Produk> getDataProdukByJudulBuku(@Param("judulBuku") String judulBuku);
    @Query("SELECT u FROM Produk AS u where u.jenisBuku=:jenisBuku")
    public List<Produk> getDataProdukByJenisBuku(@Param("jenisBuku") String jenisBuku);
    @Query("SELECT u FROM Produk AS u where u.idBuku=:idBuku")
    public Produk getDataProdukByIdBuku(@Param("idBuku") String idBuku);

    @Query(value = "SELECT * FROM Produk WHERE jumlah_buku > 0", nativeQuery = true)
    public List<Produk> getSemuaProduk();


//    BATAS SUCI
    @Query(value = "SELECT id_buku FROM Produk WHERE id_buku=:id_buku",nativeQuery = true)
    public String getIdBukuOnly(@Param("id_buku") String id_buku);

    @Query(value = "SELECT judul_buku FROM Produk WHERE judul_buku=:judul_buku", nativeQuery = true)
    public String getJudulBukuu(@Param("judul_buku") String judul_buku);


    //
    @Query("SELECT u FROM Produk AS u WHERE u.namaPengarang LIKE %:search% OR u.judulBuku LIKE %:search% OR u.jenisBuku LIKE %:search% OR u.id LIKE %:search%")
    public List<Produk> searchProduk(@Param("search") String search);

    @Query(value = "SELECT count(jenis_buku) FROM Produk WHERE jenis_buku='novel'", nativeQuery = true)
    public List<Produk> getNovel();

    @Query(value = "SELECT count(jenis_buku) FROM Produk WHERE jenis_buku='komik'", nativeQuery = true)
    public List<Produk> getKomik();

    @Query(value = "SELECT count(jenis_buku) FROM Produk WHERE jenis_buku='ensiklopedia'", nativeQuery = true)
    public List<Produk> getEnsiklopedia();

    //BATAS SUCI

    @Modifying
    @Transactional
    @Query(value = "UPDATE produk SET jumlah_buku = jumlah_buku - 1 WHERE id = :id", nativeQuery = true)
    void updateTerpinjam(@Param("id") int id);

    @Modifying
    @Transactional
    @Query(value = "UPDATE produk SET jumlah_buku = jumlah_buku + 1 WHERE id = :id", nativeQuery = true)
    void updateDikembalikan(@Param("id") int id);
}
