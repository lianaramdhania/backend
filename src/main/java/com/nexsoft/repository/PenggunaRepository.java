package com.nexsoft.repository;

import com.nexsoft.entity.Pengguna;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PenggunaRepository extends JpaRepository<Pengguna, Integer> {
    Pengguna findByUsername(String username);

    @Query("SELECT u FROM Pengguna AS u where u.id_pengguna=:id_pengguna")
    public List<Pengguna> getDataPenggunaById(@Param("id_pengguna") int id_pengguna);
    @Query("SELECT u FROM Pengguna AS u where u.username=:username")
    public List<Pengguna> getDataPenggunaByNama(@Param("username") String username);
    @Query("SELECT u FROM Pengguna AS u where u.status=:status")
    public List<Pengguna> getDataPenggunaByStatus(@Param("status") String status);
}
