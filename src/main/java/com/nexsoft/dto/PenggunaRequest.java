package com.nexsoft.dto;

import com.nexsoft.entity.Pengguna;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PenggunaRequest {
    private Pengguna pengguna;

    public Pengguna getPengguna() {
        return pengguna;
    }
}

