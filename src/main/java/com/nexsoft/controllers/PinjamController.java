package com.nexsoft.controllers;

import com.nexsoft.entity.Pengguna;
import com.nexsoft.entity.Pinjam;
import com.nexsoft.repository.LaporanRepository;
import com.nexsoft.repository.PinjamRepository;
import com.nexsoft.repository.ProdukRepository;
import com.nexsoft.service.PinjamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PinjamController {
    @Autowired
    private PinjamService pinjamService;
    @Autowired
    private PinjamRepository pinjamRepository;
    @Autowired
    private ProdukRepository produkRepository;
    @Autowired
    private LaporanRepository laporanRepository;

    @PostMapping("/addPinjam")
    public Pinjam addPinjam(@RequestBody Pinjam pinjam) {
        produkRepository.updateTerpinjam(pinjam.getProduk().getId());
        return pinjamService.savePinjam(pinjam);
    }

    @GetMapping("/getDataPinjamById/{id}")
    private List<Pinjam> getDataPinjamById(@PathVariable int id) {
        return pinjamRepository.getDataPinjamById(id);
    }

    @GetMapping("/getPinjam")
    public List<Pinjam> findAllPinjam() {
        return pinjamService.getPinjam();
    }

    @GetMapping("/getDataPinjamByIdPengguna/{pengguna_id}")
    private List<Pinjam> getDataPinjamByIdPengguna(@PathVariable("pengguna_id") int pengguna_id){
        List<Pinjam> pinjamList = pinjamRepository.getDataPinjamByIdPengguna(pengguna_id);
        System.out.println(pinjamList);
        return pinjamList;
    }

    @GetMapping("/getDataPinjamByIdPenggunadanStatus/{pengguna_id}")
    private List<Pinjam> getDataPinjamByIdPenggunadanStatus(@PathVariable int pengguna_id){
        List<Pinjam> pinjamList = pinjamRepository.getDataPinjamByIdPenggunadanStatus(pengguna_id);
        System.out.println(pinjamList);
        return pinjamList;
    }

    @GetMapping("/getJumlahProduk/{produk_id}/{pengguna_id}")
    private String getJumlahProduk(@PathVariable("produk_id") int produk_id, @PathVariable("pengguna_id") int pengguna_id){
        if (pinjamRepository.getJumlahProduk(produk_id, pengguna_id)> 0)  {
            return "Buku sudah ada";
        } else {
            return "Sukses";
        }
    }

    @DeleteMapping("/deletePinjam/{id}/{idBuku}")
    public String deletePinjamById(@PathVariable("id") int id, @PathVariable("idBuku") int idBuku) {
       produkRepository.updateDikembalikan(pinjamRepository.pilihProdukDariId(id));
        return pinjamService.deletePinjam(id, idBuku);
    }

    @PutMapping("/updatePinjam/{id_pengguna}")
    public List<Pinjam> updatePinjam(@RequestBody Pinjam pinjam, @PathVariable("id_pengguna") int idPengguna) {
        Pengguna pengguna = new Pengguna();
        pengguna.setId_pengguna(idPengguna);
        pinjam.setPengguna(pengguna);
        return pinjamService.updatePinjam(pinjam);
    }

    @GetMapping("/getPengembalian")
    private List<Pinjam> getDataPengembalian() {
        return pinjamRepository.getDataPengembalian();
    }

    @GetMapping("/getLaporanByStatusKembali")
    private List<Pinjam> getLaporanByStatus() {
        System.out.println(pinjamRepository.getLaporanByStatus());
        return pinjamRepository.getLaporanByStatus();
    }

//    @PutMapping("/updatePinjam1/{id_pengguna}")
//    public String updatePinjam1(@RequestBody Pinjam pinjam, @PathVariable("id_pengguna") int idPengguna) {
//        Pengguna pengguna = new Pengguna();
//        pengguna.setId_pengguna(idPengguna);
//        pinjam.setPengguna(pengguna);
//        return pinjamService.updatePinjam1(pinjam);
//    }

}
