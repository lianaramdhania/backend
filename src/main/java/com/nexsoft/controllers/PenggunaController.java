package com.nexsoft.controllers;

import com.nexsoft.dto.PenggunaRequest;
import com.nexsoft.dto.loginRequest;
import com.nexsoft.dto.loginResponse;
import com.nexsoft.entity.Pengguna;
import com.nexsoft.repository.PenggunaRepository;
import com.nexsoft.service.PenggunaService;
import com.nexsoft.util.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class PenggunaController {
    @Autowired
    private PenggunaService penggunaService;
    @Autowired
    private PenggunaRepository penggunaRepository;
    @Autowired
    private JWTUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/addPengguna")
    public Pengguna addPengguna(@RequestBody Pengguna pengguna) {
        return penggunaService.savePengguna(pengguna);
    }

    @PostMapping("/addPenguna")
    public Pengguna addPengguna(@RequestBody PenggunaRequest penggunaRequest) {
        return penggunaRepository.save(penggunaRequest.getPengguna());
    }

    @GetMapping("/getDataPenggunaByNama/{username}")
    private List<Pengguna> getDataPenggunaByNama(@PathVariable String username) {
        return penggunaRepository.getDataPenggunaByNama(username);
    }

    @GetMapping("/getDataPenggunaByStatus/{status}")
    private List <Pengguna> getDataPenggunaByStatus(@PathVariable String status) {
        return penggunaRepository.getDataPenggunaByStatus(status);
    }

    @GetMapping("/getDataPenggunaById/{id_pengguna}")
    private List <Pengguna> getDataPenggunaById(@PathVariable int id_pengguna) {
        return penggunaRepository.getDataPenggunaById(id_pengguna);
    }

    @DeleteMapping("/delete/{id_pengguna}")
    public String deletePenggunaById(@PathVariable int id_pengguna) {
        return penggunaService.deletePengguna(id_pengguna);
    }

    @GetMapping("/getPengguna")
    public List<Pengguna> findAllPengguna() {
        return penggunaService.getPengguna();
    }

    @PutMapping("/updatePengguna/{id_pengguna}")
    public Pengguna updatePengguna(@RequestBody Pengguna pengguna, @PathVariable int id_pengguna) {
        pengguna.setId_pengguna(id_pengguna);
        return penggunaService.updatePengguna(pengguna);
    }

    @PutMapping("/ubahPassword/{id_pengguna}")
    public Pengguna ubahPassword(@RequestBody Pengguna pengguna, @PathVariable int id_pengguna){
        pengguna.setId_pengguna(id_pengguna);
        return  penggunaService.ubahPassword(pengguna);
    }

    @PostMapping("/auth")
    public loginResponse authenticate(@RequestBody loginRequest loginRequest) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
            );
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            loginResponse failedResponse = new loginResponse();
            failedResponse.setMessage("Username and Password is not Exist!!");
            failedResponse.setStatus(403);
            failedResponse.setError("Invalid");
            return failedResponse;
        }

        return jwtUtil.generateToken(loginRequest.getUsername());

    }
}