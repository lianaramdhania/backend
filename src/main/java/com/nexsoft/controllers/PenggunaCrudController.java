package com.nexsoft.controllers;

import com.nexsoft.dto.PenggunaRequest;
import com.nexsoft.entity.Pengguna;
import com.nexsoft.repository.PenggunaRepository;
import com.nexsoft.service.PenggunaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/public")
public class PenggunaCrudController {
    @Autowired
    private PenggunaService penggunaService;
    @Autowired
    private PenggunaRepository penggunaRepository;

    @PostMapping("/addPengguna")
    public Pengguna addPengguna(@RequestBody Pengguna pengguna) {
        return penggunaService.savePengguna(pengguna);
    }

    @PostMapping("/addPenguna")
    public Pengguna addPengguna(@RequestBody PenggunaRequest penggunaRequest) {
        return penggunaRepository.save(penggunaRequest.getPengguna());
    }

    @GetMapping("/getDataPenggunaByNama/{username}")
    private List<Pengguna> getDataPenggunaByNama(@PathVariable String username) {
        return penggunaRepository.getDataPenggunaByNama(username);
    }

    @GetMapping("/getDataPenggunaByStatus/{status}")
    private List <Pengguna> getDataPenggunaByStatus(@PathVariable String status) {
        return penggunaRepository.getDataPenggunaByStatus(status);
    }

    @GetMapping("/getDataPenggunaById/{id_pengguna}")
    private List <Pengguna> getDataPenggunaById(@PathVariable int id_pengguna) {
        return penggunaRepository.getDataPenggunaById(id_pengguna);
    }

    @DeleteMapping("/delete/{id_pengguna}")
    public String deletePenggunaById(@PathVariable int id_pengguna) {
        return penggunaService.deletePengguna(id_pengguna);
    }

    @GetMapping("/getPengguna")
    public List<Pengguna> findAllPengguna() {
        return penggunaService.getPengguna();
    }

    @PutMapping("/updatePengguna/{id_pengguna}")
    public Pengguna updatePengguna(@RequestBody Pengguna pengguna, @PathVariable int id) {
        pengguna.setId_pengguna(id);
        return penggunaService.updatePengguna(pengguna);
    }
}
