package com.nexsoft.controllers;

import com.nexsoft.entity.Laporan;
import com.nexsoft.entity.Pinjam;
import com.nexsoft.repository.LaporanRepository;
import com.nexsoft.repository.PinjamRepository;
import com.nexsoft.service.LaporanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class LaporanController {
    @Autowired
    private LaporanService laporanService;
    @Autowired
    private LaporanRepository laporanRepository;
    @Autowired
    private PinjamRepository pinjamRepository;

    @PostMapping("/addLaporan/{id_pengguna}")
    public String addLaporan(@RequestBody Laporan laporan, @PathVariable("id_pengguna") int id_pengguna) {
    String respon = "";
         try{
//             laporanService.saveLaporan(laporan);
            Laporan laporan1 = laporanRepository.save(laporan);
             pinjamRepository.updateIdLaporan(laporan1.getId(), id_pengguna);
             respon = "BERHASIL";
         }
         catch (Exception e){
             e.printStackTrace();
             respon = "ERROR";
         }
         return respon;
    }

    @GetMapping("/getDataLaporanById/{id}")
    private List <Laporan> getDataLaporanById(@PathVariable("id") int id) {
        return laporanRepository.getDataLaporanById(id);
    }

    @DeleteMapping("/deleteLaporan/{id}")
    public String deleteLaporanById(@PathVariable int id) {
        return laporanService.deleteLaporan(id);
    }

    @GetMapping("/getLaporan")
    public List<Laporan> findAllLaporan() {
        System.out.println(laporanService.getLaporan());
        return laporanService.getLaporan();
    }

    @PutMapping("/updateLaporan/{id}")
    public Laporan updateLaporan(@RequestBody Laporan laporan, @PathVariable int id) {
        laporan.setId(id);
        return laporanService.updateLaporan(laporan);
    }

    @PutMapping("/pengembalian/{idPengguna}")
    public String pengembalian(@PathVariable("idPengguna") int idPengguna) throws Exception
    {
        List<Pinjam> pinjamList = pinjamRepository.getDataPinjamByIdPengguna(idPengguna);
//        produkRepository.updateDikembalikan(pinjamRepository.pilihProdukDariId(id));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss");
        Date date = sdf.parse(pinjamList.get(0).getTanggalPinjam());
        long tanggalPinjam = date.getTime();
        long tanggalKembali1 = new Date().getTime();
        long tanggalKembali = tanggalPinjam + 1000 * 60 * 60 * 24 * 5;
        long selisih = (long) Math.ceil(
                (tanggalKembali1 / (1000 * 60 * 60 * 24)) - (tanggalKembali / (1000 * 60 * 60 * 24))
        ) + 1;
        int denda = (int) (selisih * 1000);

        Laporan laporan = new Laporan();
        if(denda > 0)
        {
            laporan.setDenda(denda);
        }
        else
        {
            laporan.setDenda(0);
        }
        laporan.setTotal(pinjamList.get(0).getBiaya() + denda);
        laporan.setTanggalKembali(sdf.format(tanggalKembali1));
//        laporan.setPinjam(pinjamList.get(0).getId());
        laporanService.saveLaporan(laporan);
        pinjamRepository.updateIdLaporan(laporan.getId(),idPengguna);

        return "OK";
    }

    //Admin
    @GetMapping("/getPenghasilan/{tanggal_kembali}")
    private Integer getPenghasilan(@PathVariable("tanggal_kembali") String tanggal_kembali) {
        return laporanRepository.getPenghasilanPerhari(tanggal_kembali);
    }

    @GetMapping("/getTerpinjam/{tanggal_pinjam}")
    private Integer getBukuTerpinjam(@PathVariable("tanggal_pinjam") String tanggal_pinjam) {
        return laporanRepository.getBukuTerpinjam(tanggal_pinjam);
    }

    @GetMapping("/getDikembalikkan/{tanggal_kembali}")
    private Integer getDikembalikkan(@PathVariable("tanggal_kembali") String tanggal_kembali) {
        return laporanRepository.getBukuDikembalikkan(tanggal_kembali);
    }


    //Pengguna
    @GetMapping("/getJumlahDikembalikan/{pengguna_id}")
    private Integer getJumlahDikembalikanByIdPengguna(@PathVariable("pengguna_id") int pengguna_id) {
        return laporanRepository.getJumlahDikembalikanByIdPengguna(pengguna_id);
    }

    @GetMapping("/getJumlahPinjam/{pengguna_id}")
    private Integer getJumlahDipinjamByIdPengguna(@PathVariable("pengguna_id") int pengguna_id) {
        return laporanRepository.getJumlahDipinjamByIdPengguna(pengguna_id);
    }
}
