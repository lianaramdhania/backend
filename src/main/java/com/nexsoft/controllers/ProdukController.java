package com.nexsoft.controllers;

import com.nexsoft.entity.Produk;
import com.nexsoft.repository.ProdukRepository;
import com.nexsoft.service.ProdukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProdukController {
    @Autowired
    private ProdukService produkService;
    @Autowired
    private ProdukRepository produkRepository;

    @PostMapping("/addProduk")
    public String addProduk(@RequestBody Produk produk) {
        String response = "";
        try {
//            produk.setStatusBuku("Aktif");
            if(produkService.saveProduk(produk).equals("BERHASIL")){
                response = "BERHASIL";
            }

            else{
                response = "GAGAL TAMBAH BUKU, DATA SUDAH ADA, GANTI";
            }
        }catch (Exception e){
            response = "GAGAL TAMBAH BUKU, ID SUDAH ADA, GANTI";
            System.out.println(response);
            e.printStackTrace();
        }
        return response;
    }

    @GetMapping("/getDataProdukById/{id}")
    private List <Produk> getDataProdukById(@PathVariable int id) {
        return produkRepository.getDataProdukById(id);
    }

    @GetMapping("/getDataProdukByIdBuku/{idBuku}")
    private Produk getDataProdukByIdBuku(@PathVariable String idBuku) {
        return produkRepository.getDataProdukByIdBuku(idBuku);
    }

    @GetMapping("/getDataProdukByNamaPengarang/{namaPengarang}")
    private List<Produk> getDataProdukByNamaPengarang(@PathVariable String namaPengarang) {
        return produkRepository.getDataProdukByNamaPengarang(namaPengarang);
    }

    @GetMapping("/getDataProdukByJudulBuku/{judulBuku}")
    private List <Produk> getDataProdukByJudulBuku(@PathVariable String judulBuku) {
        return produkRepository.getDataProdukByJudulBuku(judulBuku);
    }

    @GetMapping("/getDataProdukByJenisBuku/{jenisBuku}")
    private List <Produk> getDataProdukByJenisBuku(@PathVariable String jenisBuku) {
        return produkRepository.getDataProdukByJenisBuku(jenisBuku);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteProdukById(@PathVariable int id) {
        return produkService.deleteProduk(id);
    }

    @GetMapping("/getProduk")
    public List<Produk> findAllProduk() {
        return produkService.getProduk();
    }

    @GetMapping("/searchProduk")
    public List<Produk> searchProduk(@RequestParam("search") String search)
    { return produkService.searchProduk(search);
    }

    @PutMapping("/updateProduk/{id}")
    public Produk updateProduk(@RequestBody Produk produk, @PathVariable int id) {
        produk.setId(id);
        return produkService.updateProduk(produk);
    }

    @PutMapping("/hapusBuku/{id}")
    public String hapusProduk(@RequestBody Produk produk, @PathVariable int id){
        produk.getId();
        return produkService.hapusProduk(produk);
    }

}
